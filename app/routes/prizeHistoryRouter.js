const express = require("express");

const router = express.Router();

const {
 createPrizeHistory,
  getAllPrizeHistory,
  getPrizeHistoryById,
  updatePrizeHistoryById,
  deletePrizeHistoryById
} = require("../controllers/prizeHistoryController");

router.post("/prize-histories", createPrizeHistory);

router.get("/prize-histories", getAllPrizeHistory);

router.get("/prize-histories/:prizeHistoryId", getPrizeHistoryById);

router.put("/prize-histories/:prizeHistoryId", updatePrizeHistoryById);

router.delete("/prize-histories/:prizeHistoryId",deletePrizeHistoryById);

module.exports = router;
