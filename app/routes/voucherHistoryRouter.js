const express = require("express");

const router = express.Router();

const {
 createVoucherHistory,
  getAllVoucherHistory,
  getVoucherHistoryById,
  updateVoucherHistoryById,
  deleteVoucherHistoryById
} = require("../controllers/voucherHistoryController");

router.post("/voucher-histories", createVoucherHistory);

router.get("/voucher-histories", getAllVoucherHistory);

router.get("/voucher-histories/:voucherHistoryId", getVoucherHistoryById);

router.put("/voucher-histories/:voucherHistoryId", updateVoucherHistoryById);

router.delete("/voucher-histories/:voucherHistoryId",deleteVoucherHistoryById);

module.exports = router;
