// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Voucher Model
const voucherModel = require("../models/voucherModel");

// Create Voucher
const createVoucher = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let body = req.body;

 // B2: Validate dữ liệu
 if(!body.code) {
     return res.status(400).json({
         status: "Error 400: Bad Request",
         message: "Name is required!"
     })
 }
 if(!isNaN(body.code)) {
  return res.status(400).json({
     status: "Error 400: Bad Request",
     message: "Name must be String!"
  })
 }
 if(!body.discount) {
  return res.status(400).json({
      status: "Error 400: Bad Request",
      message: "Discount is required!"
  })
}
 if(!(Number.isInteger(body.discount) && body.discount > 0 && body.discount <= 100 )) {
  return res.status(400).json({
      status: "Error 400: Bad Request",
      message: "Discount is invalid!"
  })
}
 

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 let newVoucherData = {
     _id: mongoose.Types.ObjectId(),
     code: body.code,
     discount: body.discount,
     note: body.note
 }

 voucherModel.create(newVoucherData, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(201).json({
         message: "Create successfully",
         newVoucher: data
     })
 })
}

// Get all Voucher 
const getAllVoucher = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 // B2: Validate dữ liệu
 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 voucherModel.find((error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(200).json({
         message: "Get all Voucher successfully",
         Vouchers: data
     })
 })
}

// Get Voucher by id
const getVoucherById = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let voucherId = req.params.voucherId;

 // B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(voucherId)) {
     return res.status(400).json({
         status: "Error 400: Bad Request",
         message: "Voucher ID is invalid!"
     })
 }

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 voucherModel.findById(voucherId, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(201).json({
         message: `Get Vouchers ID ${voucherId} successfully`,
         Voucher: data
     })
 })
}

// Update Voucher by id
const updateVoucherById = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let voucherId = req.params.voucherId;
 let body = req.body;

 // B2: Validate dữ liệu
if (mongoose.Types.ObjectId.isValid(voucherId) ) {
 
}

if(!isNaN(body.code)) {
return res.status(400).json({
  status: "Error 400: Bad Request",
  message: "Name must be String!"
})
}
if(!(Number.isInteger(body.discount) && body.discount > 0 && body.discount <= 100 )) {
return res.status(400).json({
   status: "Error 400: Bad Request",
   message: "Discount is invalid!"
})
}

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 let VoucherUpdate = {
   //_id: mongoose.Types.ObjectId(),
   code: body.code,
   discount: body.discount,
   note: body.note
 };

 voucherModel.findByIdAndUpdate(voucherId, VoucherUpdate, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(200).json({
         message: "Update Voucher successfully",
         updatedVoucher: data
     })
 })
}

// Delete Voucher by id
const deleteVoucherById = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let voucherId = req.params.voucherId;

 // B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(voucherId)) {
     return res.status(400).json({
      status: "Error 400: Bad Request",
         message: "Voucher ID is invalid!"
     })
 }

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 voucherModel.findByIdAndDelete(voucherId, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(204).json({
         message: "Delete Voucher successfully"
     })
 })
}

// Export Voucher controller thành 1 module
module.exports = {
   createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}
