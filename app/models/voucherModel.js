//import thư viện mongoose
const mongoose = require("mongoose");

//Khai báo class Schema của mongoose
const Schema = mongoose.Schema;

//Khai báo course Schema
const voucherSchema = new Schema({
 // _id:{
 //  type:mongoose.Types.ObjectId,
 //  unique:true
 // },
 code:{
  type:String,
  required:true,
  unique:true
 },
 discount:{
  type:Number,
  required:true
 },
 note:{
  type:String,
  required:false
 }
},{
 timestamps:true
});

module.exports = mongoose.model("Voucher",voucherSchema);