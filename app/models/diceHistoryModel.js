//import thư viện mongoose
const mongoose = require("mongoose");


//Khai báo class Schema của mongoose
const Schema = mongoose.Schema;

//Khai báo course Schema
const diceHistorySchema = new Schema({
 // _id:{
 //  type:mongoose.Types.ObjectId,
 //  unique:true
 // },
 user:[
  {
   type:mongoose.Types.ObjectId,
   ref:"User",
   required:true
  }
 ],
 dice:{
  type:Number,
  required:true
 },
},{
 timestamps:true
});

module.exports = mongoose.model("DiceHistory",diceHistorySchema);